import 'package:flutter/material.dart';
import 'package:techo_flutter/models/Event.dart';

class EventCard extends StatelessWidget {
  const EventCard(
      {Key? key,
      required this.titleCard,
      required this.backImage,
      required this.chips,
      required this.eventInfo})
      : super(key: key);

  final String titleCard;

  final String backImage;

  final List<String> chips;

  final Event eventInfo;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.lightBlue,
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, '/event-detail', arguments: eventInfo);
        },
        child: Column(
          children: [
            SizedBox(
              height: 130,
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Image.network(
                      '$backImage',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    bottom: 12,
                    left: 16,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerLeft,
                      child: Text('$titleCard',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30)),
                    ),
                  ),
                  Positioned(
                    bottom: 50,
                    left: 12,
                    child: Row(
                      children: [
                        for (var c in chips)
                          EventChip(
                            chipText: c,
                          )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class EventChip extends StatelessWidget {
  const EventChip({Key? key, required this.chipText}) : super(key: key);

  final String chipText;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Card(
        color: Theme.of(context).primaryColorLight,
        margin: EdgeInsets.fromLTRB(3, 1, 3, 1),
        child: Padding(
          padding: EdgeInsets.fromLTRB(8, 1, 8, 1),
          child: Text(
            "$chipText",
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12),
          ),
        ),
      ),
    );
  }
}
