import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:light/light.dart';
import 'package:meta/meta.dart';
import '../theme_helper.dart';
import 'package:equatable/equatable.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  late Light _light;

  num _luxInt = 0;

  late StreamSubscription _subscription;

  ThemeBloc() : super(ThemeBloc.initialState);

  @override
  static ThemeState get initialState =>
      ThemeState(themeData: ThemeHelper.appThemeData[AppTheme.BlueLight]);

  @override
  Stream<ThemeState> mapEventToState(ThemeEvent event) async* {
    if (event is ThemeChanged) {
      var temaActual = state.themeData;
      if (event.allowContext) {
        _initContextThemeState();
      } else {
        if (event.theme == null) {
          if (temaActual == ThemeHelper.appThemeData[AppTheme.BlueLight]) {
            temaActual = ThemeHelper.appThemeData[AppTheme.BlueDark];
          } else {
            temaActual = ThemeHelper.appThemeData[AppTheme.BlueLight];
          }
        } else {
          temaActual = ThemeHelper.appThemeData[event.theme];
        }
      }
      yield ThemeState(themeData: temaActual);
    }
  }

  Future<void> _initContextThemeState() async {
    startListening();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (exception) {
      print(exception);
    }
  }

  void onData(int luxValue) async {
    print("lux value: $luxValue");
    if (_luxInt < 150) {
      this.add(ThemeChanged(theme: AppTheme.BlueDark));
    } else {
      this.add(ThemeChanged(theme: AppTheme.BlueLight));
    }
    _luxInt = luxValue;
  }
}
