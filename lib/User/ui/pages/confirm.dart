import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/material.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:techo_flutter/models/ModelProvider.dart';
import 'package:techo_flutter/models/User.dart';
import 'package:techo_flutter/utils/models/signup_data.dart';

class ConfirmScreen extends StatefulWidget {
  // Data from login
  final LoginData data;

  // Constructor
  ConfirmScreen({required this.data});

  // State constructor
  @override
  _ConfirmScreenState createState() => _ConfirmScreenState();
}

class _ConfirmScreenState extends State<ConfirmScreen> {
  // Controlador que se asegura que no este vacio para comprobar
  final _controller = TextEditingController();
  // Boolean que indica que no este vacio el texto
  bool _isEnabled = false;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      setState(() {
        _isEnabled = _controller.text.isNotEmpty;
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _verifyCode(BuildContext context, SignupData data, String code) async {
    try {
      final res = await Amplify.Auth.confirmSignUp(
        username: data.name,
        confirmationCode: code,
      );

      if (res.isSignUpComplete) {
        // Login user
        final user = await Amplify.Auth.signIn(
            username: data.name, password: data.password);

        final userDB = User(
          email: data.email,
          attendances: [],
          birthday: data.birthday,
          city: data.city,
          dni: data.dni,
          name: data.userName,
          phone: data.phone,
          role: UserRole.VOLUNTEER,
        );

        await Amplify.DataStore.save(userDB);

        if (user.isSignedIn) {
          Navigator.pushReplacementNamed(context, '/dashboard');
        }
      }
    } on AuthException catch (e) {
      _showError(context, e.message);
    } on DataStoreException catch (e) {
      _showError(
          context, "Usuario registrado. Problema en servidor. " + e.message);
    } catch (e) {
      print("Error on Singup confirmation: $e");
      _showError(context, "Error. Comunícate con un asistente técnico.");
    }
  }

  void _resendCode(BuildContext context, LoginData data) async {
    try {
      await Amplify.Auth.resendSignUpCode(username: data.name);

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.blueAccent,
          content: Text(
              'Código de confirmación enviado. Por favor revise su email',
              style: TextStyle(fontSize: 15)),
        ),
      );
    } on AuthException catch (e) {
      _showError(context, e.message);
    }
  }

  void _showError(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Colors.redAccent,
        content: Text(
          message,
          style: TextStyle(fontSize: 15),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
          child: SafeArea(
              minimum: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Card(
                    elevation: 12,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    margin: const EdgeInsets.all(30),
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        children: [
                          SizedBox(height: 10),
                          TextField(
                              controller: _controller,
                              decoration: InputDecoration(
                                  filled: true,
                                  contentPadding:
                                      const EdgeInsets.symmetric(vertical: 4.0),
                                  prefixIcon: Icon(Icons.lock),
                                  labelText:
                                      'Ingrese su código de confirmación',
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(40))))),
                          SizedBox(height: 10),
                          MaterialButton(
                            onPressed: _isEnabled
                                ? () {
                                    _verifyCode(
                                        context,
                                        SignupData.fromLoginData(widget.data),
                                        _controller.text);
                                  }
                                : null,
                            elevation: 4,
                            color: Theme.of(context).primaryColor,
                            disabledColor: Colors.blue.shade100,
                            child: Text('Verificar',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14)),
                          ),
                          MaterialButton(
                            onPressed: () {
                              _resendCode(context, widget.data);
                            },
                            child: Text('Reenviar código',
                                style: TextStyle(color: Colors.grey)),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ))),
    );
  }
}
