import json
import boto3
import datetime
from boto3.dynamodb.conditions import Key, Attr

client = boto3.client('dynamodb', region_name="us-east-1")

TABLE_NAME = "Attendance-i52hyaiobfeqdpntkhsqm7wxya-dev"


def calculate_distance(event_location, user_location):
    ''''
    Calculates the distance between two {lat, lng} points.
    The distance is measured in meters.
    '''

    return 1000000


def get_attendance_object(event_id, user_id):
    data = client.query(
        TableName=TABLE_NAME,
        IndexName='byEvent',
        KeyConditionExpression="eventID = :eventId",
        FilterExpression="userID = :userId",
        ExpressionAttributeValues={
            ":eventId": {"S": event_id},
            ":userId": {"S": user_id},
        }
    )
    print("Attendance object:", data)
    return data["Items"][0]


def get_event_location(event_id):
    data = client.get_item(
        TableName=TABLE_NAME,
        Key={
            "id": {
                "S": event_id
            }
        },
        AttributesToGet=[
            'latitude',
            'longitude',
        ],
    )
    return {
        "lat": data["Item"]['latitude'],
        "lng": data["Item"]['longitude'],
    }


def attend_attendance(attendance):
    response = client.update_item(
        TableName=TABLE_NAME,
        Key={
            'id': attendance["id"],
        },
        AttributeUpdates={
            'attended': {'Value': {'BOOL': True}},
            'attendedDateTime': {'Value': {'S': datetime.datetime.now().isoformat()}},
        },
        ReturnValues="UPDATED_NEW"
    )
    print("Update object:", response)
    return response


def get_event_id(qr_data):
    data_parts = str(qr_data).split("TECHO_APP_ATTDCE_E-")
    return data_parts[1]


def handler(event, context):
    print('received event:')
    print(event)

    user_id = event.get("userID", None)
    qr_data = event.get("qrData", None)
    user_lat = event.get("userLat", None)
    user_lng = event.get("userLng", None)

    statusCode = 400
    message = "No se logró tomar la asistencia"

    try:
        event_id = get_event_id(qr_data=qr_data)
        try:
            attendance = get_attendance_object(
                event_id=event_id, user_id=user_id)
            try:
                attend_attendance(attendance=attendance)
                statusCode = 200
                message = "Asistencia registrada correctamente."
            except Exception as e3:
                message = "Ocurrió un problema en el servidor. Intenta más tarde."
                print("Error marcando la asistencia", e3)
        except Exception as e2:
            message = "El evento o el usuario no existe. O el usuario no está registrado en el evento."
            print("Error trayendo objeto Attendance", e2)
    except Exception as e1:
        message = "El código QR es incorrecto."
        print("Error leyendo los datos de QR", e1)

    return {
        'statusCode': statusCode,
        'headers': {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': json.dumps(message)
    }
