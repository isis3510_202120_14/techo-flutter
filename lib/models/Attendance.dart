/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, file_names, unnecessary_new, prefer_if_null_operators, prefer_const_constructors, slash_for_doc_comments, annotate_overrides, non_constant_identifier_names, unnecessary_string_interpolations, prefer_adjacent_string_concatenation, unnecessary_const, dead_code

import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Attendance type in your schema. */
@immutable
class Attendance extends Model {
  static const classType = const _AttendanceModelType();
  final String id;
  final String? _eventID;
  final String? _userID;
  final bool? _attended;
  final TemporalDateTime? _attendedDateTime;
  final bool? _mandatoryEvent;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get eventID {
    return _eventID;
  }
  
  String? get userID {
    return _userID;
  }
  
  bool? get attended {
    return _attended;
  }
  
  TemporalDateTime? get attendedDateTime {
    return _attendedDateTime;
  }
  
  bool? get mandatoryEvent {
    return _mandatoryEvent;
  }
  
  const Attendance._internal({required this.id, eventID, userID, attended, attendedDateTime, mandatoryEvent}): _eventID = eventID, _userID = userID, _attended = attended, _attendedDateTime = attendedDateTime, _mandatoryEvent = mandatoryEvent;
  
  factory Attendance({String? id, String? eventID, String? userID, bool? attended, TemporalDateTime? attendedDateTime, bool? mandatoryEvent}) {
    return Attendance._internal(
      id: id == null ? UUID.getUUID() : id,
      eventID: eventID,
      userID: userID,
      attended: attended,
      attendedDateTime: attendedDateTime,
      mandatoryEvent: mandatoryEvent);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Attendance &&
      id == other.id &&
      _eventID == other._eventID &&
      _userID == other._userID &&
      _attended == other._attended &&
      _attendedDateTime == other._attendedDateTime &&
      _mandatoryEvent == other._mandatoryEvent;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Attendance {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("eventID=" + "$_eventID" + ", ");
    buffer.write("userID=" + "$_userID" + ", ");
    buffer.write("attended=" + (_attended != null ? _attended!.toString() : "null") + ", ");
    buffer.write("attendedDateTime=" + (_attendedDateTime != null ? _attendedDateTime!.format() : "null") + ", ");
    buffer.write("mandatoryEvent=" + (_mandatoryEvent != null ? _mandatoryEvent!.toString() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Attendance copyWith({String? id, String? eventID, String? userID, bool? attended, TemporalDateTime? attendedDateTime, bool? mandatoryEvent}) {
    return Attendance(
      id: id ?? this.id,
      eventID: eventID ?? this.eventID,
      userID: userID ?? this.userID,
      attended: attended ?? this.attended,
      attendedDateTime: attendedDateTime ?? this.attendedDateTime,
      mandatoryEvent: mandatoryEvent ?? this.mandatoryEvent);
  }
  
  Attendance.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _eventID = json['eventID'],
      _userID = json['userID'],
      _attended = json['attended'],
      _attendedDateTime = json['attendedDateTime'] != null ? TemporalDateTime.fromString(json['attendedDateTime']) : null,
      _mandatoryEvent = json['mandatoryEvent'];
  
  Map<String, dynamic> toJson() => {
    'id': id, 'eventID': _eventID, 'userID': _userID, 'attended': _attended, 'attendedDateTime': _attendedDateTime?.format(), 'mandatoryEvent': _mandatoryEvent
  };

  static final QueryField ID = QueryField(fieldName: "attendance.id");
  static final QueryField EVENTID = QueryField(fieldName: "eventID");
  static final QueryField USERID = QueryField(fieldName: "userID");
  static final QueryField ATTENDED = QueryField(fieldName: "attended");
  static final QueryField ATTENDEDDATETIME = QueryField(fieldName: "attendedDateTime");
  static final QueryField MANDATORYEVENT = QueryField(fieldName: "mandatoryEvent");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Attendance";
    modelSchemaDefinition.pluralName = "Attendances";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PRIVATE,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Attendance.EVENTID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Attendance.USERID,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Attendance.ATTENDED,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.bool)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Attendance.ATTENDEDDATETIME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Attendance.MANDATORYEVENT,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.bool)
    ));
  });
}

class _AttendanceModelType extends ModelType<Attendance> {
  const _AttendanceModelType();
  
  @override
  Attendance fromJson(Map<String, dynamic> jsonData) {
    return Attendance.fromJson(jsonData);
  }
}