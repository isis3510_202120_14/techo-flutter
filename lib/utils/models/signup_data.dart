import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter_login/flutter_login.dart';

class SignupData extends LoginData {
  final String? userName;
  final String email;
  final String? city;
  final String password;
  final TemporalDate? birthday;
  final String? dni;
  final String? phone;

  SignupData({
    required this.email,
    required this.password,
    this.userName,
    this.city,
    this.birthday,
    this.dni,
    this.phone,
  }) : super(
          name: email,
          password: password,
        );

  factory SignupData.fromLoginData(LoginData data) => SignupData(
        email: data.name,
        password: data.password,
      );
}
