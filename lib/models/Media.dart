/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, file_names, unnecessary_new, prefer_if_null_operators, prefer_const_constructors, slash_for_doc_comments, annotate_overrides, non_constant_identifier_names, unnecessary_string_interpolations, prefer_adjacent_string_concatenation, unnecessary_const, dead_code

import 'ModelProvider.dart';
import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Media type in your schema. */
@immutable
class Media extends Model {
  static const classType = const _MediaModelType();
  final String id;
  final MediaType? _type;
  final String? _url;
  final List<EventMedia>? _events;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  MediaType? get type {
    return _type;
  }
  
  String? get url {
    return _url;
  }
  
  List<EventMedia>? get events {
    return _events;
  }
  
  const Media._internal({required this.id, type, url, events}): _type = type, _url = url, _events = events;
  
  factory Media({String? id, MediaType? type, String? url, List<EventMedia>? events}) {
    return Media._internal(
      id: id == null ? UUID.getUUID() : id,
      type: type,
      url: url,
      events: events != null ? List<EventMedia>.unmodifiable(events) : events);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Media &&
      id == other.id &&
      _type == other._type &&
      _url == other._url &&
      DeepCollectionEquality().equals(_events, other._events);
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Media {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("type=" + (_type != null ? enumToString(_type)! : "null") + ", ");
    buffer.write("url=" + "$_url");
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Media copyWith({String? id, MediaType? type, String? url, List<EventMedia>? events}) {
    return Media(
      id: id ?? this.id,
      type: type ?? this.type,
      url: url ?? this.url,
      events: events ?? this.events);
  }
  
  Media.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _type = enumFromString<MediaType>(json['type'], MediaType.values),
      _url = json['url'],
      _events = json['events'] is List
        ? (json['events'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => EventMedia.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'type': enumToString(_type), 'url': _url, 'events': _events?.map((EventMedia? e) => e?.toJson()).toList()
  };

  static final QueryField ID = QueryField(fieldName: "media.id");
  static final QueryField TYPE = QueryField(fieldName: "type");
  static final QueryField URL = QueryField(fieldName: "url");
  static final QueryField EVENTS = QueryField(
    fieldName: "events",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (EventMedia).toString()));
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Media";
    modelSchemaDefinition.pluralName = "Media";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PRIVATE,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Media.TYPE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.enumeration)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Media.URL,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Media.EVENTS,
      isRequired: false,
      ofModelName: (EventMedia).toString(),
      associatedKey: EventMedia.MEDIA
    ));
  });
}

class _MediaModelType extends ModelType<Media> {
  const _MediaModelType();
  
  @override
  Media fromJson(Map<String, dynamic> jsonData) {
    return Media.fromJson(jsonData);
  }
}