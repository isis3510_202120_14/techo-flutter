import 'package:flutter/material.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:flutter_login/flutter_login.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  // Informacion del login del usuario
  LoginData? _data;
  bool _isSignedIn = false;

  Future<String?> _onLogin(LoginData data) async {
    try {
      final res = await Amplify.Auth.signIn(
        username: data.name,
        password: data.password,
      );
      _isSignedIn = res.isSignedIn;
      return null;
    } on AuthException catch (e) {
      if (e.message.contains('There is already a user signed in.')) {
        await Amplify.Auth.signOut();
        return 'Problema iniciando sesión. Por favor, intenta de nuevo.';
      }
      if (e.message.contains('Failed since user is not authorized.')) {
        return 'Credenciales incorrectas. Por favor, intenta de nuevo.';
      }
      return '${e.message} - ${e.recoverySuggestion}';
    }
  }

  Future<String?> _onRecoverPassword(BuildContext context, String email) async {
    try {
      final res = await Amplify.Auth.resetPassword(username: email);

      if (res.nextStep.updateStep == 'CONFIRM_RESET_PASSWORD_WITH_CODE') {
        Navigator.of(context).pushReplacementNamed(
          '/confirm-reset',
          arguments: LoginData(name: email, password: ''),
        );
      }
      return null;
    } on AuthException catch (e) {
      return '${e.message} - ${e.recoverySuggestion}';
    }
  }

  Future<String?> _onSingup(LoginData data) async {
    try {
      await Amplify.Auth.signUp(
          username: data.name,
          password: data.password,
          options: CognitoSignUpOptions(userAttributes: {'email': data.name}));
      // Store the response of the user
      _data = data;
      return null;
    } on AuthException catch (e) {
      if (e.message.contains('The password given is invalid.')) {
        return 'La contraseña proporcionada no es válida. Debe tener un mínimo de 8 caracteres, letras mayúsculas, minúsculas, caracteres especiales y números en su interior.';
      } else if (e.message.contains('Username already exists in the system.')) {
        return 'Ya existe un usuario con ese correo, por favor recupera tu cuenta.';
      }
      return 'There was a problem signing up. Please try again';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      logo: 'assets/imgs/techo_white_logo.png',
      onLogin: _onLogin,
      onRecoverPassword: (String email) => _onRecoverPassword(context, email),
      onSignup: _onSingup,
      theme: LoginTheme(primaryColor: Theme.of(context).primaryColor),
      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacementNamed(
          _isSignedIn ? '/dashboard' : '/confirm',
          arguments: _data,
        );
      },
      messages: LoginMessages(
        userHint: 'Usuario',
        passwordHint: 'Contraseña',
        confirmPasswordHint: 'Confirmar Contraseña',
        loginButton: 'Iniciar sesión',
        signupButton: '¡Quiero ser voluntario!',
        goBackButton: 'Ir atras',
        confirmPasswordError: '¡Las contraseñas no coinciden!',
        forgotPasswordButton: '¿Olvidaste tu contraseña?',
        recoverPasswordDescription:
            'Ingresa el correo electrónico de tu cuenta TECHO asociada para definir una nueva contraseña',
        recoverPasswordButton: 'Recuperar',
        recoverPasswordSuccess: 'Contraseña recuperada con éxito',
        recoverPasswordIntro: 'Recupera tu cuenta aquí',
        flushbarTitleError: '¡Error!',
        signUpSuccess: '¡Te has registrado con éxito!',
        flushbarTitleSuccess: '¡Éxito!',
        providersTitle: 'o inicia sesión con',
      ),
    );
  }
}
