/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, file_names, unnecessary_new, prefer_if_null_operators, prefer_const_constructors, slash_for_doc_comments, annotate_overrides, non_constant_identifier_names, unnecessary_string_interpolations, prefer_adjacent_string_concatenation, unnecessary_const, dead_code

import 'ModelProvider.dart';
import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the User type in your schema. */
@immutable
class User extends Model {
  static const classType = const _UserModelType();
  final String id;
  final String? _name;
  final String? _email;
  final UserRole? _role;
  final List<Attendance>? _attendances;
  final String? _city;
  final TemporalDate? _birthday;
  final String? _dni;
  final String? _phone;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  String? get name {
    return _name;
  }
  
  String? get email {
    return _email;
  }
  
  UserRole? get role {
    return _role;
  }
  
  List<Attendance>? get attendances {
    return _attendances;
  }
  
  String? get city {
    return _city;
  }
  
  TemporalDate? get birthday {
    return _birthday;
  }
  
  String? get dni {
    return _dni;
  }
  
  String? get phone {
    return _phone;
  }
  
  const User._internal({required this.id, name, email, role, attendances, city, birthday, dni, phone}): _name = name, _email = email, _role = role, _attendances = attendances, _city = city, _birthday = birthday, _dni = dni, _phone = phone;
  
  factory User({String? id, String? name, String? email, UserRole? role, List<Attendance>? attendances, String? city, TemporalDate? birthday, String? dni, String? phone}) {
    return User._internal(
      id: id == null ? UUID.getUUID() : id,
      name: name,
      email: email,
      role: role,
      attendances: attendances != null ? List<Attendance>.unmodifiable(attendances) : attendances,
      city: city,
      birthday: birthday,
      dni: dni,
      phone: phone);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
      id == other.id &&
      _name == other._name &&
      _email == other._email &&
      _role == other._role &&
      DeepCollectionEquality().equals(_attendances, other._attendances) &&
      _city == other._city &&
      _birthday == other._birthday &&
      _dni == other._dni &&
      _phone == other._phone;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("User {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("name=" + "$_name" + ", ");
    buffer.write("email=" + "$_email" + ", ");
    buffer.write("role=" + (_role != null ? enumToString(_role)! : "null") + ", ");
    buffer.write("city=" + "$_city" + ", ");
    buffer.write("birthday=" + (_birthday != null ? _birthday!.format() : "null") + ", ");
    buffer.write("dni=" + "$_dni" + ", ");
    buffer.write("phone=" + "$_phone");
    buffer.write("}");
    
    return buffer.toString();
  }
  
  User copyWith({String? id, String? name, String? email, UserRole? role, List<Attendance>? attendances, String? city, TemporalDate? birthday, String? dni, String? phone}) {
    return User(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      role: role ?? this.role,
      attendances: attendances ?? this.attendances,
      city: city ?? this.city,
      birthday: birthday ?? this.birthday,
      dni: dni ?? this.dni,
      phone: phone ?? this.phone);
  }
  
  User.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _name = json['name'],
      _email = json['email'],
      _role = enumFromString<UserRole>(json['role'], UserRole.values),
      _attendances = json['attendances'] is List
        ? (json['attendances'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => Attendance.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _city = json['city'],
      _birthday = json['birthday'] != null ? TemporalDate.fromString(json['birthday']) : null,
      _dni = json['dni'],
      _phone = json['phone'];
  
  Map<String, dynamic> toJson() => {
    'id': id, 'name': _name, 'email': _email, 'role': enumToString(_role), 'attendances': _attendances?.map((Attendance? e) => e?.toJson()).toList(), 'city': _city, 'birthday': _birthday?.format(), 'dni': _dni, 'phone': _phone
  };

  static final QueryField ID = QueryField(fieldName: "user.id");
  static final QueryField NAME = QueryField(fieldName: "name");
  static final QueryField EMAIL = QueryField(fieldName: "email");
  static final QueryField ROLE = QueryField(fieldName: "role");
  static final QueryField ATTENDANCES = QueryField(
    fieldName: "attendances",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Attendance).toString()));
  static final QueryField CITY = QueryField(fieldName: "city");
  static final QueryField BIRTHDAY = QueryField(fieldName: "birthday");
  static final QueryField DNI = QueryField(fieldName: "dni");
  static final QueryField PHONE = QueryField(fieldName: "phone");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "User";
    modelSchemaDefinition.pluralName = "Users";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.OWNER,
        ownerField: "owner",
        identityClaim: "cognito:username",
        operations: [
          ModelOperation.CREATE,
          ModelOperation.DELETE,
          ModelOperation.UPDATE
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.NAME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.EMAIL,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.ROLE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.enumeration)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: User.ATTENDANCES,
      isRequired: false,
      ofModelName: (Attendance).toString(),
      associatedKey: Attendance.USERID
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.CITY,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.BIRTHDAY,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.date)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.DNI,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: User.PHONE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
  });
}

class _UserModelType extends ModelType<User> {
  const _UserModelType();
  
  @override
  User fromJson(Map<String, dynamic> jsonData) {
    return User.fromJson(jsonData);
  }
}