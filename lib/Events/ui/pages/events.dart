import 'dart:ui';
import 'package:amplify_flutter/amplify.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techo_flutter/models/ModelProvider.dart';
import 'package:techo_flutter/utils/widgets/home_top_bar_cubit.dart';
import 'package:techo_flutter/utils/widgets/topbar.dart';

import '../widgets/event_card.dart';
import 'package:flutter/material.dart';

const String techoLogoLink =
    'https://www.techo.org/wp-content/uploads/2019/04/Color-RGB-Sin-fondo-02.png';

List events = ["Card 1", "Card 2"];
List<String> chipsL = ["Chip 1", "Chip 2"];
List<Event> eventsList = [];
List<Attendance> attendanceList = [];
List<List<Media>> mediaImg = [];
List<EventMedia> evMedias = [];
Event recomEvent = Event();

class EventsTecho extends StatelessWidget {
  const EventsTecho({Key? key}) : super(key: key);

  // TODO Check better ways to query the info and the queries.
  Future<List<Event>?> getEvents() async {
    try {
      int mostPopular = 0;
      eventsList = await Amplify.DataStore.query(Event.classType);
      List<Media> mediaImgAux = [];
      int len = 0;
      for (var ev in eventsList) {
        evMedias = await Amplify.DataStore.query(EventMedia.classType,
            where: EventMedia.EVENT.eq(ev.id));
        for (var item in evMedias) {
          mediaImgAux.add(item.media);
        }
        mediaImg.add(mediaImgAux);
        mediaImgAux = [];
        attendanceList = await Amplify.DataStore.query(Attendance.classType,
            where: Attendance.EVENTID.eq(ev.id));
        len = attendanceList.length;
        if (len > mostPopular) {
          recomEvent = ev;
          mostPopular = len;
        }
      }
      print(mostPopular);
      print(recomEvent);
      return eventsList;
    } catch (e) {
      print("Could not query DataStore: " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<HomeTopBarCubit>(context).setTopBar(
      TechoTopBar(
        showLogo: true,
        optionList: [
          "Configuración",
          "Ayuda",
          "Acerca de Techo",
          "Salir de la app",
          "Cambiar Tema",
          "Cambiar Tema con contexto"
        ],
        onNotification: () => showModalBottomSheet(
          context: context,
          builder: (context) => Text("Notificaciones"),
        ),
        onAdd: () => showModalBottomSheet(
          context: context,
          builder: (context) => Text("Crear Evento"),
        ),
      ),
    );
    return FutureBuilder(
      future: getEvents(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(12),
            children: [
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                child: Text(
                  "Evento recomendado",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
              ),
              EventCard(
                titleCard: recomEvent.name!,
                backImage: mediaImg[0][0].url!,
                chips: recomEvent.tags!,
                eventInfo: recomEvent,
              ),
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.fromLTRB(0, 5, 0, 10),
                child: Text(
                  "Tus eventos",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                ),
              ),
              for (int i = 0; i < eventsList.length; i++)
                EventCard(
                  titleCard: eventsList[i].name!,
                  backImage: mediaImg[i][0].url!,
                  chips: eventsList[i].tags!,
                  eventInfo: eventsList[i],
                )
            ],
          );
        } else if (snapshot.hasError) {
          // return widget de mensaje de error
          print("Ha ocurrido un error");
        }
        // retornar un CircularProgressIndac
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
