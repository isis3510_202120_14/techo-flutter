import 'package:amplify_flutter/amplify.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techo_flutter/theme/bloc/theme_bloc.dart';

class TechoTopBar extends StatelessWidget with PreferredSizeWidget {
  final bool goBack;
  final bool showLogo;
  final Function()? onNotification;
  final Function()? onAdd;
  // TODO Generar una clase para el menú de opciones donde tenga una lista de nombre - function
  final List<String> optionList;
  const TechoTopBar({
    Key? key,
    this.goBack = false,
    this.showLogo = false,
    this.onAdd,
    this.onNotification,
    this.optionList = const [],
  }) : super(key: key);

  void _goBack(BuildContext context) {
    Navigator.of(context).pop();
  }

  Widget? _createBackButton(BuildContext context) {
    return goBack
        ? InkWell(
            onTap: () => _goBack(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          )
        : null;
  }

  Widget _createNotificationButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: InkWell(
        onTap: onNotification,
        child: Icon(
          Icons.notifications_outlined,
          size: 28,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _createAddButton(BuildContext context) {
    return InkWell(
      onTap: onAdd,
      child: Icon(
        Icons.add,
        size: 28,
        color: Colors.white,
      ),
    );
  }

  Widget _createOptionMenu(BuildContext context) {
    return PopupMenuButton(
      color: Colors.white,
      itemBuilder: (context) => List.generate(
        optionList.length,
        (index) => PopupMenuItem<String>(
          value: optionList[index],
          child: Text(
            optionList[index],
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ),
      onSelected: (item) {
        if (item == 'Salir de la app') {
          Amplify.Auth.signOut().then((_) {
            Navigator.pushReplacementNamed(context, '/');
          });
        } else if (item == 'Cambiar Tema') {
          BlocProvider.of<ThemeBloc>(context).add(ThemeChanged());
        } else if (item == 'Cambiar Tema con contexto') {
          BlocProvider.of<ThemeBloc>(context)
              .add(ThemeChanged(allowContext: true));
        } else {
          print(item);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: _createBackButton(context),
      title: showLogo
          ? Text("TECHO")
          : null, //TODO Cambiar a imagen de logo de techo y con el condicional
      actions: [
        if (onNotification != null) _createNotificationButton(context),
        if (onAdd != null) _createAddButton(context),
        if (optionList.isNotEmpty) _createOptionMenu(context),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
