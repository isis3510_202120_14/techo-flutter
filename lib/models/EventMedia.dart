/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, file_names, unnecessary_new, prefer_if_null_operators, prefer_const_constructors, slash_for_doc_comments, annotate_overrides, non_constant_identifier_names, unnecessary_string_interpolations, prefer_adjacent_string_concatenation, unnecessary_const, dead_code

import 'ModelProvider.dart';
import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the EventMedia type in your schema. */
@immutable
class EventMedia extends Model {
  static const classType = const _EventMediaModelType();
  final String id;
  final Event? _event;
  final Media? _media;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  Event get event {
    try {
      return _event!;
    } catch(e) {
      throw new DataStoreException(DataStoreExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage, recoverySuggestion: DataStoreExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion, underlyingException: e.toString());
    }
  }
  
  Media get media {
    try {
      return _media!;
    } catch(e) {
      throw new DataStoreException(DataStoreExceptionMessages.codeGenRequiredFieldForceCastExceptionMessage, recoverySuggestion: DataStoreExceptionMessages.codeGenRequiredFieldForceCastRecoverySuggestion, underlyingException: e.toString());
    }
  }
  
  const EventMedia._internal({required this.id, required event, required media}): _event = event, _media = media;
  
  factory EventMedia({String? id, required Event event, required Media media}) {
    return EventMedia._internal(
      id: id == null ? UUID.getUUID() : id,
      event: event,
      media: media);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventMedia &&
      id == other.id &&
      _event == other._event &&
      _media == other._media;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("EventMedia {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("event=" + (_event != null ? _event!.toString() : "null") + ", ");
    buffer.write("media=" + (_media != null ? _media!.toString() : "null"));
    buffer.write("}");
    
    return buffer.toString();
  }
  
  EventMedia copyWith({String? id, Event? event, Media? media}) {
    return EventMedia(
      id: id ?? this.id,
      event: event ?? this.event,
      media: media ?? this.media);
  }
  
  EventMedia.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _event = json['event']?['serializedData'] != null
        ? Event.fromJson(new Map<String, dynamic>.from(json['event']['serializedData']))
        : null,
      _media = json['media']?['serializedData'] != null
        ? Media.fromJson(new Map<String, dynamic>.from(json['media']['serializedData']))
        : null;
  
  Map<String, dynamic> toJson() => {
    'id': id, 'event': _event?.toJson(), 'media': _media?.toJson()
  };

  static final QueryField ID = QueryField(fieldName: "eventMedia.id");
  static final QueryField EVENT = QueryField(
    fieldName: "event",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Event).toString()));
  static final QueryField MEDIA = QueryField(
    fieldName: "media",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Media).toString()));
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "EventMedia";
    modelSchemaDefinition.pluralName = "EventMedias";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PRIVATE,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ]),
      AuthRule(
        authStrategy: AuthStrategy.PRIVATE,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.belongsTo(
      key: EventMedia.EVENT,
      isRequired: true,
      targetName: "eventID",
      ofModelName: (Event).toString()
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.belongsTo(
      key: EventMedia.MEDIA,
      isRequired: true,
      targetName: "mediaID",
      ofModelName: (Media).toString()
    ));
  });
}

class _EventMediaModelType extends ModelType<EventMedia> {
  const _EventMediaModelType();
  
  @override
  EventMedia fromJson(Map<String, dynamic> jsonData) {
    return EventMedia.fromJson(jsonData);
  }
}