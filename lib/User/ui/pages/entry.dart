import 'dart:async';

import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:light/light.dart';
import 'package:techo_flutter/theme/bloc/theme_bloc.dart';
import 'package:techo_flutter/theme/theme_helper.dart';

import '../widgets/login.dart';

class EntryScreen extends StatefulWidget {
  @override
  _EntryScreenState createState() => _EntryScreenState();
}

class _EntryScreenState extends State<EntryScreen> {
  late Light _light;

  String _luxString = 'unknown';
  late StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Login(),
      ),
    );
  }

  void _checkLogin() async {
    try {
      await Amplify.Auth.getCurrentUser();
      //send user to dashboard
      Navigator.of(context).pushReplacementNamed('/dashboard');
    } on AuthException catch (e) {
      //send user to login
      print('Usuario no logueado');
    }
  }
}
