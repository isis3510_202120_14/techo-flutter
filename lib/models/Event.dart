/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, file_names, unnecessary_new, prefer_if_null_operators, prefer_const_constructors, slash_for_doc_comments, annotate_overrides, non_constant_identifier_names, unnecessary_string_interpolations, prefer_adjacent_string_concatenation, unnecessary_const, dead_code

import 'ModelProvider.dart';
import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';


/** This is an auto generated class representing the Event type in your schema. */
@immutable
class Event extends Model {
  static const classType = const _EventModelType();
  final String id;
  final List<Attendance>? _attendances;
  final String? _description;
  final String? _name;
  final TemporalDateTime? _startDateTime;
  final TemporalDateTime? _endDateTime;
  final List<EventMedia>? _media;
  final String? _qrCode;
  final double? _latitude;
  final double? _longitude;
  final bool? _online;
  final List<String>? _tags;
  final String? _location;

  @override
  getInstanceType() => classType;
  
  @override
  String getId() {
    return id;
  }
  
  List<Attendance>? get attendances {
    return _attendances;
  }
  
  String? get description {
    return _description;
  }
  
  String? get name {
    return _name;
  }
  
  TemporalDateTime? get startDateTime {
    return _startDateTime;
  }
  
  TemporalDateTime? get endDateTime {
    return _endDateTime;
  }
  
  List<EventMedia>? get media {
    return _media;
  }
  
  String? get qrCode {
    return _qrCode;
  }
  
  double? get latitude {
    return _latitude;
  }
  
  double? get longitude {
    return _longitude;
  }
  
  bool? get online {
    return _online;
  }
  
  List<String>? get tags {
    return _tags;
  }
  
  String? get location {
    return _location;
  }
  
  const Event._internal({required this.id, attendances, description, name, startDateTime, endDateTime, media, qrCode, latitude, longitude, online, tags, location}): _attendances = attendances, _description = description, _name = name, _startDateTime = startDateTime, _endDateTime = endDateTime, _media = media, _qrCode = qrCode, _latitude = latitude, _longitude = longitude, _online = online, _tags = tags, _location = location;
  
  factory Event({String? id, List<Attendance>? attendances, String? description, String? name, TemporalDateTime? startDateTime, TemporalDateTime? endDateTime, List<EventMedia>? media, String? qrCode, double? latitude, double? longitude, bool? online, List<String>? tags, String? location}) {
    return Event._internal(
      id: id == null ? UUID.getUUID() : id,
      attendances: attendances != null ? List<Attendance>.unmodifiable(attendances) : attendances,
      description: description,
      name: name,
      startDateTime: startDateTime,
      endDateTime: endDateTime,
      media: media != null ? List<EventMedia>.unmodifiable(media) : media,
      qrCode: qrCode,
      latitude: latitude,
      longitude: longitude,
      online: online,
      tags: tags != null ? List<String>.unmodifiable(tags) : tags,
      location: location);
  }
  
  bool equals(Object other) {
    return this == other;
  }
  
  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Event &&
      id == other.id &&
      DeepCollectionEquality().equals(_attendances, other._attendances) &&
      _description == other._description &&
      _name == other._name &&
      _startDateTime == other._startDateTime &&
      _endDateTime == other._endDateTime &&
      DeepCollectionEquality().equals(_media, other._media) &&
      _qrCode == other._qrCode &&
      _latitude == other._latitude &&
      _longitude == other._longitude &&
      _online == other._online &&
      DeepCollectionEquality().equals(_tags, other._tags) &&
      _location == other._location;
  }
  
  @override
  int get hashCode => toString().hashCode;
  
  @override
  String toString() {
    var buffer = new StringBuffer();
    
    buffer.write("Event {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("description=" + "$_description" + ", ");
    buffer.write("name=" + "$_name" + ", ");
    buffer.write("startDateTime=" + (_startDateTime != null ? _startDateTime!.format() : "null") + ", ");
    buffer.write("endDateTime=" + (_endDateTime != null ? _endDateTime!.format() : "null") + ", ");
    buffer.write("qrCode=" + "$_qrCode" + ", ");
    buffer.write("latitude=" + (_latitude != null ? _latitude!.toString() : "null") + ", ");
    buffer.write("longitude=" + (_longitude != null ? _longitude!.toString() : "null") + ", ");
    buffer.write("online=" + (_online != null ? _online!.toString() : "null") + ", ");
    buffer.write("tags=" + (_tags != null ? _tags!.toString() : "null") + ", ");
    buffer.write("location=" + "$_location");
    buffer.write("}");
    
    return buffer.toString();
  }
  
  Event copyWith({String? id, List<Attendance>? attendances, String? description, String? name, TemporalDateTime? startDateTime, TemporalDateTime? endDateTime, List<EventMedia>? media, String? qrCode, double? latitude, double? longitude, bool? online, List<String>? tags, String? location}) {
    return Event(
      id: id ?? this.id,
      attendances: attendances ?? this.attendances,
      description: description ?? this.description,
      name: name ?? this.name,
      startDateTime: startDateTime ?? this.startDateTime,
      endDateTime: endDateTime ?? this.endDateTime,
      media: media ?? this.media,
      qrCode: qrCode ?? this.qrCode,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      online: online ?? this.online,
      tags: tags ?? this.tags,
      location: location ?? this.location);
  }
  
  Event.fromJson(Map<String, dynamic> json)  
    : id = json['id'],
      _attendances = json['attendances'] is List
        ? (json['attendances'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => Attendance.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _description = json['description'],
      _name = json['name'],
      _startDateTime = json['startDateTime'] != null ? TemporalDateTime.fromString(json['startDateTime']) : null,
      _endDateTime = json['endDateTime'] != null ? TemporalDateTime.fromString(json['endDateTime']) : null,
      _media = json['media'] is List
        ? (json['media'] as List)
          .where((e) => e?['serializedData'] != null)
          .map((e) => EventMedia.fromJson(new Map<String, dynamic>.from(e['serializedData'])))
          .toList()
        : null,
      _qrCode = json['qrCode'],
      _latitude = (json['latitude'] as num?)?.toDouble(),
      _longitude = (json['longitude'] as num?)?.toDouble(),
      _online = json['online'],
      _tags = json['tags']?.cast<String>(),
      _location = json['location'];
  
  Map<String, dynamic> toJson() => {
    'id': id, 'attendances': _attendances?.map((Attendance? e) => e?.toJson()).toList(), 'description': _description, 'name': _name, 'startDateTime': _startDateTime?.format(), 'endDateTime': _endDateTime?.format(), 'media': _media?.map((EventMedia? e) => e?.toJson()).toList(), 'qrCode': _qrCode, 'latitude': _latitude, 'longitude': _longitude, 'online': _online, 'tags': _tags, 'location': _location
  };

  static final QueryField ID = QueryField(fieldName: "event.id");
  static final QueryField ATTENDANCES = QueryField(
    fieldName: "attendances",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (Attendance).toString()));
  static final QueryField DESCRIPTION = QueryField(fieldName: "description");
  static final QueryField NAME = QueryField(fieldName: "name");
  static final QueryField STARTDATETIME = QueryField(fieldName: "startDateTime");
  static final QueryField ENDDATETIME = QueryField(fieldName: "endDateTime");
  static final QueryField MEDIA = QueryField(
    fieldName: "media",
    fieldType: ModelFieldType(ModelFieldTypeEnum.model, ofModelName: (EventMedia).toString()));
  static final QueryField QRCODE = QueryField(fieldName: "qrCode");
  static final QueryField LATITUDE = QueryField(fieldName: "latitude");
  static final QueryField LONGITUDE = QueryField(fieldName: "longitude");
  static final QueryField ONLINE = QueryField(fieldName: "online");
  static final QueryField TAGS = QueryField(fieldName: "tags");
  static final QueryField LOCATION = QueryField(fieldName: "location");
  static var schema = Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Event";
    modelSchemaDefinition.pluralName = "Events";
    
    modelSchemaDefinition.authRules = [
      AuthRule(
        authStrategy: AuthStrategy.PRIVATE,
        operations: [
          ModelOperation.CREATE,
          ModelOperation.UPDATE,
          ModelOperation.DELETE,
          ModelOperation.READ
        ])
    ];
    
    modelSchemaDefinition.addField(ModelFieldDefinition.id());
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Event.ATTENDANCES,
      isRequired: false,
      ofModelName: (Attendance).toString(),
      associatedKey: Attendance.EVENTID
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.DESCRIPTION,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.NAME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.STARTDATETIME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.ENDDATETIME,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.dateTime)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.hasMany(
      key: Event.MEDIA,
      isRequired: false,
      ofModelName: (EventMedia).toString(),
      associatedKey: EventMedia.EVENT
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.QRCODE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.LATITUDE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.LONGITUDE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.double)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.ONLINE,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.bool)
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.TAGS,
      isRequired: false,
      isArray: true,
      ofType: ModelFieldType(ModelFieldTypeEnum.collection, ofModelName: describeEnum(ModelFieldTypeEnum.string))
    ));
    
    modelSchemaDefinition.addField(ModelFieldDefinition.field(
      key: Event.LOCATION,
      isRequired: false,
      ofType: ModelFieldType(ModelFieldTypeEnum.string)
    ));
  });
}

class _EventModelType extends ModelType<Event> {
  const _EventModelType();
  
  @override
  Event fromJson(Map<String, dynamic> jsonData) {
    return Event.fromJson(jsonData);
  }
}