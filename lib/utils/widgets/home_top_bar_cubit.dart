import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techo_flutter/utils/widgets/topbar.dart';

class HomeTopBarCubit extends Cubit<HomeTopBarState> {
  HomeTopBarCubit() : super(HomeTopBarState());

  void setTopBar(PreferredSizeWidget topBar) {
    emit(HomeTopBarState(topBar: topBar));
  }
}

class HomeTopBarState {
  static const defaultTopBar = TechoTopBar();

  final PreferredSizeWidget topBar;

  const HomeTopBarState({this.topBar = defaultTopBar});
}
