import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:techo_flutter/Events/ui/widgets/attendance_modal.dart';
import 'package:techo_flutter/models/ModelProvider.dart';
import 'package:techo_flutter/utils/widgets/topbar.dart';

class EventDetailScreen extends StatelessWidget {
  final Event? event;
  const EventDetailScreen({Key? key, this.event}) : super(key: key);

  Widget _createBanner(BuildContext context) {
    return Material(
      elevation: 2,
      child: Container(
        width: double.infinity,
        height: 220,
        child: Stack(
          children: [
            Positioned.fill(
              child: ColorFiltered(
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.2), BlendMode.srcATop),
                child: CachedNetworkImage(
                  imageUrl:
                      "https://f.hubspotusercontent00.net/hubfs/5721605/25%20Ways%20to%20Volunteer%20in%20Your%20Community.jpg",
                  fit: BoxFit.cover,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      Center(
                    child: CircularProgressIndicator(
                        value: downloadProgress.progress),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      event?.name ?? "Jornada de clases",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 34,
                        height: 2.5,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    Text(
                      event?.location ?? "Comunidad de San Cristóbal",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _createEventData(BuildContext context) {
    final tags = event?.tags ??
        [
          "Niños y adolescentes",
          "Educación",
        ];
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 20, 12, 12),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 23,
            child: ListView.builder(
              itemBuilder: (context, index) => Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(right: 8),
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.blueGrey[200]!.withOpacity(0.6),
                ),
                child: Text(
                  tags[index],
                  style: TextStyle(fontSize: 10),
                ),
              ),
              itemCount: tags.length,
              scrollDirection: Axis.horizontal,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.calendar_today,
                size: 26,
                color: Colors.black54,
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                event?.startDateTime?.format()?.split("T")[0] ??
                    "Sábado, 17 de Septiembre de 2021",
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.alarm,
                color: Colors.black54,
                size: 26,
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                event?.startDateTime?.format()?.split("T")[1] ??
                    "05:00 a.m. - 12:00 p.m.",
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // TODO Cambiar todos los color: Colors.black54 por variables del theme global

  Widget _createDescription(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 8,
      ),
      child: Text(
        event?.description ??
            "Estaremos en jornada de clases en la comunidad de San Cristóbal. Las clases se dividirán en 3 secciones: teoría de manejo de equipos, práctica y actividades lúdicas, charlas comunitarias.",
        style: TextStyle(
          fontSize: 14,
          height: 1.8,
          color: Colors.black54,
        ),
      ),
    );
  }

  Widget _createGallery(BuildContext context) {
    final images = [
      "https://f.hubspotusercontent00.net/hubfs/5721605/25%20Ways%20to%20Volunteer%20in%20Your%20Community.jpg",
      "https://ec.europa.eu/education/sites/default/files/community-volunteering-1200x800.jpg",
      "https://media.bizj.us/view/img/10273770/howtovounteer*1200xx3865-2178-0-177.jpg",
      "https://www.stonyplain.com/en/live/resources/b-volunteering.jpg",
      "https://www.roberthalf.com/sites/default/files/styles/full_width_content_image_1x_small_480/public/Media_Root/images/blog-mr/rhmr_blog_volunteering_1115.jpg?itok=W1it4gXy",
    ];
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: SizedBox(
        height: 180,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => SizedBox(
            height: 180,
            width: 180,
            child: Material(
              elevation: 5,
              child: CachedNetworkImage(
                imageUrl: images[index],
                fit: BoxFit.cover,
                progressIndicatorBuilder: (context, url, downloadProgress) =>
                    Center(
                        child: CircularProgressIndicator(
                            value: downloadProgress.progress)),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ),
          separatorBuilder: (context, index) => SizedBox(
            width: 12,
          ),
          itemCount: images.length,
        ),
      ),
    );
  }

  Widget _createAttendanceButton(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      padding: const EdgeInsets.all(18),
      child: ElevatedButton(
        child: Text(
          "Registrar asistencia",
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () => showModalBottomSheet(
          isScrollControlled: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12),
              topRight: Radius.circular(12),
            ),
          ),
          context: context,
          builder: (context) => Wrap(
            children: [
              AttendanceModal(
                eventID: event?.id,
              ),
            ],
          ),
        ),
      ), //TODO Poner la lógica de la asistencia
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TechoTopBar(
        goBack: true,
        onAdd: () => showDialog(
          context: context,
          builder: (context) => Text("Hola"),
        ),
        showLogo: true,
        optionList: ["Ayuda"],
      ),
      body: ListView(
        children: [
          _createBanner(context),
          _createEventData(context),
          Divider(
            color: Colors.black38,
          ),
          _createDescription(context),
          Divider(
            color: Colors.black38,
          ),
          _createGallery(context),
          Divider(
            color: Colors.black38,
          ),
          _createAttendanceButton(context),
        ],
      ),
    );
  }
}
