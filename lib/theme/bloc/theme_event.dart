part of 'theme_bloc.dart';

abstract class ThemeEvent extends Equatable {
  // Passing class fields in a list to the Equatable super class
  ThemeEvent([List props = const <dynamic>[]]);
  @override
  List<Object> get props => [];
}

class ThemeChanged extends ThemeEvent {
  final AppTheme? theme;
  final bool allowContext;

  ThemeChanged({this.theme, this.allowContext = false})
      : super([theme, allowContext]);
}
