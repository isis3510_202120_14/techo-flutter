import 'package:amplify_flutter/amplify.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techo_flutter/Events/ui/pages/event_detail_screen.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:techo_flutter/User/ui/pages/confirm.dart';
import 'package:techo_flutter/User/ui/pages/confirm_reset.dart';
import 'package:techo_flutter/User/ui/pages/entry.dart';
import 'package:techo_flutter/home.dart';
import 'package:techo_flutter/models/ModelProvider.dart';
import 'package:techo_flutter/theme/bloc/theme_bloc.dart';
import 'package:techo_flutter/utils/configure_amplify.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureAmplify();
  runApp(TechoApp());
}

class TechoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ThemeBloc(),
      child: BlocBuilder<ThemeBloc, ThemeState>(builder: _buildWithTheme),
    );
  }

  Widget _buildWithTheme(BuildContext context, ThemeState state) {
    return MaterialApp(
      title: 'Techo',
      theme: state.themeData,
      home: EntryScreen(),
      onGenerateRoute: (settings) {
        if (settings.name == '/confirm') {
          return PageRouteBuilder(
              pageBuilder: (_, __, ___) =>
                  ConfirmScreen(data: settings.arguments as LoginData),
              transitionsBuilder: (_, __, ___, child) => child);
        }
        if (settings.name == '/dashboard') {
          return PageRouteBuilder(
              pageBuilder: (_, __, ___) => HomePage(),
              transitionsBuilder: (_, __, ___, child) => child);
        }
        if (settings.name == '/event-detail') {
          return PageRouteBuilder(
              pageBuilder: (_, __, ___) => EventDetailScreen(
                    event: settings.arguments as Event,
                  ),
              transitionsBuilder: (_, __, ___, child) => child);
        }
        if (settings.name == '/confirm-reset') {
          return PageRouteBuilder(
            pageBuilder: (_, __, ___) =>
                ConfirmResetScreen(data: settings.arguments as LoginData),
            transitionsBuilder: (_, __, ___, child) => child,
          );
        }
        return MaterialPageRoute(builder: (_) => EntryScreen());
      },
    );
  }
}
