import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:techo_flutter/utils/widgets/home_top_bar_cubit.dart';
import 'Events/ui/pages/events.dart';
import 'utils/widgets/bottom_bar.dart';

const _kPages = <String, List>{
  'Eventos': [Icons.home, EventsTecho()],
  'Tareas': [Icons.check, SizedBox.shrink()],
  'Cursos': [Icons.book, SizedBox.shrink()],
  'Perfil': [Icons.person, SizedBox.shrink()]
};

class HomePage extends StatefulWidget implements PreferredSizeWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();

  @override
  Size get preferredSize => const Size.fromHeight(80);
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeTopBarCubit(),
      child: DefaultTabController(
        length: 4,
        initialIndex: 0,
        child: BlocBuilder<HomeTopBarCubit, HomeTopBarState>(
            builder: (context, state) {
          return Scaffold(
            appBar: state.topBar,
            body: Column(
              children: [
                const Divider(),
                Expanded(
                    child: TabBarView(
                  children: [
                    for (final icon in _kPages.values)
                      icon[1], //Poner pantallas
                  ],
                ))
              ],
            ),
            bottomNavigationBar: BottomBarTecho(),
          );
        }),
      ),
    );
  }
}
