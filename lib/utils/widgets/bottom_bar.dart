import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import '../../Events/ui/pages/events.dart';

const _kPages = <String, List>{
  'Eventos': [Icons.home, EventsTecho()],
  'Tareas': [Icons.check, Icons.check],
  'Cursos': [Icons.book, Icons.book],
  'Perfil': [Icons.person, Icons.person]
};

class BottomBarTecho extends StatefulWidget implements PreferredSizeWidget {
  BottomBarTecho({Key? key}) : super(key: key);

  @override
  _BottomBarTechoState createState() => _BottomBarTechoState();

  @override
  Size get preferredSize => const Size.fromHeight(80);
}

class _BottomBarTechoState extends State<BottomBarTecho> {
  TabStyle _tabStyle = TabStyle.reactCircle;

  @override
  Widget build(BuildContext context) {
    return ConvexAppBar(
      style: _tabStyle,
      backgroundColor: Color.fromRGBO(0, 146, 221, 1),
      color: Colors.white,
      items: <TabItem>[
        for (final entry in _kPages.entries)
          TabItem(icon: entry.value[0], title: entry.key)
      ],
      //onTap: (int i) => print(i)
    );
  }
}
