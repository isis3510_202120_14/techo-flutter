import 'dart:typed_data';

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'dart:developer';
import 'dart:io';

import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/foundation.dart';
import 'package:techo_flutter/models/User.dart';

class AttendanceModal extends StatefulWidget {
  final String? eventID;
  AttendanceModal({Key? key, this.eventID}) : super(key: key);

  @override
  _AttendanceModalState createState() => _AttendanceModalState();
}

class _AttendanceModalState extends State<AttendanceModal> {
  Barcode? result;
  QRViewController? controller;
  String? scanMessage = "Escaneando...";
  bool _reading = true;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.blue,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      if (scanData.code.startsWith("TECHO_APP_ATTDCE_E") &&
          scanData.code.endsWith(widget.eventID ?? "")) {
        _reading = false;
      } else {
        scanMessage = "Código no válido";
      }
      setState(() {
        result = scanData;
      });
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('no Permission')),
      );
    }
  }

  Future<List> _takeAttendance(String qrData) async {
    var message = "Error al tomar asistencia";
    var success = false;
    try {
      final user = await Amplify.Auth.getCurrentUser();
      final userDB = await Amplify.DataStore.query(User.classType,
          where: User.EMAIL.eq(user.username));
      RestOptions options = RestOptions(
        path: '/take-attendance',
        body: Uint8List.fromList(
            '{\'userID\':\'${userDB.first.id}\', \'qrData\':\'$qrData\'}'
                .codeUnits),
      );
      RestOperation restOperation = Amplify.API.post(restOptions: options);
      RestResponse response = await restOperation.response;
      if (response.statusCode == 200) {
        success = true;
        message = "Asistencia registrada correctamente";
      } else if (response.statusCode == 400) {
        message = String.fromCharCodes(response.data);
      }

      print(new String.fromCharCodes(response.data));
    } on ApiException catch (e) {
      print('POST call failed: $e');
      message = "Ocurrió un erro. Intenta más tarde.";
    } catch (e) {
      message = "El usuario no existe";
      print("Error on attendance taking: $e");
    }
    return [success, message];
  }

  Widget _readQRWidgets() => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 230,
            child: Text(
              "Escanea el código QR del evento para registrar la asistencia",
              style: TextStyle(
                color: Colors.grey[600],
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(18),
            child: SizedBox(
              height: 280,
              width: 280,
              child: _buildQrView(context),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(scanMessage ?? "Escaneando..."),
          ),
          ElevatedButton(
            child: Text(
              "Cancelar",
              style: TextStyle(color: Colors.grey[600]),
            ),
            onPressed: () => Navigator.of(context).pop(),
            style: ElevatedButton.styleFrom(
              primary: Colors.grey[300],
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            ),
          ),
        ],
      );

  Widget _resultWidget() => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 230,
            child: Text(
              "Resultado de la asistencia",
              style: TextStyle(
                color: Colors.grey[600],
              ),
              textAlign: TextAlign.center,
            ),
          ),
          FutureBuilder(
              future: _takeAttendance(result!.code),
              builder: (context, snapshot) {
                if (snapshot.hasData && (snapshot.data as List)[0]) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.check_circle,
                        size: 48,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.all(16),
                        child: Text(
                          (snapshot.data as List)[1],
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.grey[600],
                          ),
                        ),
                      ),
                      ElevatedButton(
                        child: Text(
                          "Listo",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    ],
                  );
                } else if (snapshot.hasData && (snapshot.data as List)[0]) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outlined,
                        size: 48,
                        color: Colors.red[400],
                      ),
                      Padding(
                        padding: EdgeInsets.all(16),
                        child: Text(
                          (snapshot.data as List)[1],
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.red[600],
                          ),
                        ),
                      ),
                      ElevatedButton(
                        child: Text(
                          "Cancelar",
                          style: TextStyle(color: Colors.grey[600]),
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.grey[300],
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 10),
                        ),
                      ),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outlined,
                        size: 48,
                        color: Colors.red[400],
                      ),
                      Padding(
                        padding: EdgeInsets.all(16),
                        child: Text(
                          "Error. Intenta más tarde.",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.red[600],
                          ),
                        ),
                      ),
                      ElevatedButton(
                        child: Text(
                          "Cancelar",
                          style: TextStyle(color: Colors.grey[600]),
                        ),
                        onPressed: () => Navigator.of(context).pop(),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.grey[300],
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 10),
                        ),
                      ),
                    ],
                  );
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              })
        ],
      );

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.black38,
              borderRadius: BorderRadius.circular(6),
            ),
            height: 6,
            width: 48,
            margin: const EdgeInsets.all(6),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Registro de asistencia",
              style: TextStyle(
                fontSize: 24,
                color: Colors.grey[700],
              ),
            ),
          ),
          _reading ? _readQRWidgets() : _resultWidget(),
          SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }
}
